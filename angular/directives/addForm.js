angular.module('locations')
  .directive('addform', function() {
    return {
      restrict: 'E',
      templateUrl: '/angular/templates/directives/addForm.html',
    };
  });
