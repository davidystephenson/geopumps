angular.module('locations', ['ngRoute'])
  .config(function($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'angular/templates/home.html',
        controller: 'homeController',
      })
      .when('/view', {
        templateUrl: 'angular/templates/view.html',
        controller: 'viewController',
      })
      .when('/add', {
        templateUrl: 'angular/templates/add.html',
      });

	  $locationProvider.html5Mode({enabled: true, requireBase: false});
  });
