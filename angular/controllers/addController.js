angular.module('locations').controller('addController', function($scope, $http, locationsService) {
  $scope.submitForm = function(valid, location) {
    if (valid) {
      $http.post('/locations', location)
        .success(function(data) {
          console.log('Location successfully saved:', data);
          locationsService.locations = data;
        })
        .error(function(err) { console.log(err); });
    } else {
      throw new FormError('The submitted form was invalid');
    }
  };

  // Get the list of states via AJAX, simulating gathering external data from APIs.
  $http.get('/states')
    .success(function(data) { $scope.states = data; })
    .error(function(err) { console.log(err); });

  // Get the list of countries via AJAX, simulating gathering external data from APIs.
  $http.get('/countries')
    .success(function(data) { $scope.countries = data; })
    .error(function(err) { console.log(err); });
});
