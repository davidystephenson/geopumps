angular.module('locations').controller('viewController', function($scope, $http, locationsService) {
  $scope.locationsService = locationsService;

  $scope.adding = false;

  // Update the shared list of locations
  $http.get('/locations').then(function(response) {
    locationsService.locations = response.data;
  });
});
