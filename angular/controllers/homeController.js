angular.module('locations').controller('homeController', function($scope, $http, locationsService) {
  $scope.locationsService = locationsService;
  
  // Update the shared list of locations
  $http.get('/locations').then(function(response) {
    locationsService.locations = response.data;

    // Sort the array and get the location with the lowest price
    locationsService.locations.sort(function(a, b) {
      return a.price - b.price;
    });

    $scope.best = locationsService.locations[0];

    // Format the price into US Dollars
    var numeric = parseFloat($scope.best.price);

    $scope.price = numeric.toLocaleString(
      'USD',
      { style: 'currency', currency: "USD", minimumFractionDigits: 2, maximumFractionDigits: 2 }
    );
  });
});
