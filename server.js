// Dependencies
var bodyParser = require('body-parser');
var express = require('express');

// Initialize application variables
var app = express();
var port = process.env.PORT || 9799;

// Configure Express
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');

// Get files for Angular
app.use('/angular', express.static(__dirname + '/angular'));
app.use('/public', express.static(__dirname + '/public'));

// Connect Express routes
require('./app/routes')(app);

// Create some application variables. A database would be used in a production environment.
app.locations = [
  {
    address: '123 Main Street',
    city: 'Arlington',
    state: 'VA',
    zip: '22201',
    price: 2.49,
  },
  {
    address: '456 Keystone Blvd',
    city: 'Fairfax',
    state: 'VA',
    zip: '22030',
    price: 2.47,
  },
  {
    address: '789 Ember Avenue',
    city: 'Leesburg',
    state: 'VA',
    price: 2.40,
  }
];

app.states = require('./app/states');
app.countries = require('./app/countries');

// Launch application
app.listen(port);
console.log('Server listening on port:', port);

module.exports = app;
