var request = require('request');
var path = require('path');
var keys = require('../config/secret/keys');

var geolocate = function(location, callback) {
  // Geocode the location using the Texas A&M Geoservices API.
  // Too slow for production, but nice for developing.
  var base = 'http://geoservices.tamu.edu/Services/Geocode/WebService/';
  var type = 'GeocoderWebServiceHttpNonParsed_V04_01.aspx?';
  var address = 'streetAddress=' + encodeURI(location.address);
  var city = '&city=' + encodeURI(location.city);
  var state = '&state=' + encodeURI(location.state);
  var key = '&apikey=' + keys.tamu;
  var format = '&format=json&version=4.01';
  var uri = base + type + address + city + state + key + format;

  request(uri, function(error, response, body) {
    if (error) return callback(error, body);

    if (response.statusCode !== 200) {
      var statusError = new InvalidStatusError(
        'Invalid status code returned from TAMU:' + response.statusCode
      );
      return( callback(statusError, body) );
    }

    var data = JSON.parse(body);

    var geocode = data.OutputGeocodes[0].OutputGeocode;

    location.latitude = geocode.Latitude;
    location.longitude = geocode.Longitude;

    callback(null, location);
  });
};

var price = function(location, callback) {
  // Get local gas prices using the MyGasFeed API.
  var base = 'http://devapi.mygasfeed.com/stations/radius/';
  var parameters = '/5/reg/price/' + keys.gas + '.json';
  var uri = base + location.latitude + '/' + location.longitude + parameters;

  request(uri, function(error, response, body) {
    if (error) return( callback(error, body) );

    if (response.statusCode !== 200) {
      statusError = InvalidStatusError(
        'Invalid status code returned from MyGasFeed:' + response.statusCode
      );
      return( callback(statusError, body) );
    }

    var data = JSON.parse(body);

    for (var i = 0; i < data.stations.length; i ++ ) {
      if (data.stations[i].reg_price !== 'N\/A') location.price = data.stations[i].reg_price;
    }

    callback(null, location);
  });

};

module.exports = function(app) {
  app.get('/states', function(req, res) { res.send(app.states); });

  app.get('/countries', function(req, res) { res.send(app.countries); });

  app.get('/locations', function(req, res) { res.send(app.locations); });

  app.post('/locations', function(req, res) { 
    geolocate(req.body, function(error, location) {
      if (error) return console.log(error);

      else {
        price(location, function(error, location) {
          if (error) return console.log(error);

          // Add the location to the application
          app.locations.push(req.body);

          // Return the updated list of locations
          res.send(app.locations);
        });
      }
    });

  });

  // Wildcard route for angular
  app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname, '../angular', 'index.html'));
  });
};
